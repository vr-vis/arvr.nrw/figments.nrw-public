## Summary

(Summarize the bug encountered concisely)

## Steps to reproduce

(How one can reproduce the issue - this is very important)

## Version

(Which version of Figments.nrw are you using? Installed or Archive?)

## Mode
(Does the error happen in Desktop Mode, VR Mode or both?)

## What is the current bug behavior?

(What actually happens)

## What is the expected correct behavior?

(What you should see instead)

## Relevant logs and/or screenshots

(Paste any relevant logs - use code blocks (```) to format console output, logs, and code, as
it's very hard to read otherwise.)

## Possible fixes

(If you have any ideas for a bug fix, let us know!)
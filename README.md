> :warning: **Repository moved**
> 
> Please note that the public repository of Figments.nrw has moved to servers of the University of Wuppertal:
>
> [**Public Repository Figments.nrw** (BUW)](https://git.uni-wuppertal.de/tmdt-iel/figments.nrw)
> <hr>
> The repository you currently are viewing (hosted at the RWTH) is no longer maintained and information contained is outdated.

---


# Figments.nrw

Figments.nrw is a free and open source authoring tool for virtual reality in education. This project aims to provide educators and learners with a powerful platform to create immersive educational experiences using virtual reality technology.

**Note:** The repository currently contains only issues and releases. The source code for Figments.nrw will be published at a later date.

## Table of Contents

- [Introduction](#figmentsnrw)
- [Getting Started](#getting-started)
- [Reporting Issues](#reporting-issues)
- [Wiki](#wiki)

## Getting Started

To get started with Figments.nrw, follow these steps:

1. Download the latest installer from the [Releases](https://git-ce.rwth-aachen.de/vr-vis/arvr.nrw/figments.nrw-public/-/releases) page.

2. Install Figments.nrw using the downloaded installer.

3. Head over to our [wiki](https://git-ce.rwth-aachen.de/vr-vis/arvr.nrw/figments.nrw-public/-/wikis/home) or [www.figments.nrw](https://www.figments.nrw) for tutorials and how-tos.

4. Begin creating your VR educational experiences!

## Reporting Issues

If you encounter any bugs, issues, or have suggestions for improvements, please report them on our [Issue Tracker](https://git-ce.rwth-aachen.de/vr-vis/arvr.nrw/figments.nrw-public/-/issues). To ensure efficient issue resolution, please provide detailed information about the problem, steps to reproduce it, and any relevant screenshots.

## Wiki

For additional information, guidelines, and usage instructions, refer to our [Wiki](https://git-ce.rwth-aachen.de/vr-vis/arvr.nrw/figments.nrw-public/-/wikis/home).